
var dibpsData = null;

function dibpsDataInitialize() {
    dibpsData = {
        "device": {
            "keyUuid": null,
            "keySeedB64": null,
            "keySeedBytes": null,
            "keyPairSign": null,
            "keyPairCrypt": null,
            "publicKey64": null,
            "bookmark": null
        },
        "section": {
            "id": 1,
            "name": "sectionDevice"
        },
        "input": {
            "challengeB64": null,
            "verifyTokenB64": null,
            "regTokenB64": null,
            "authToken": null,
            "cardNumber": null
        },
        "output": {
            "challengeSignatureB64": null,
            "authTokenSignatureB64": null,
            "cardNumberSignatureB64": null,
            "verifyTokenSignatureFromStringB64": null,
            "verifyTokenSignatureFromBytesB64": null,
            "verifierB64": null,
            "verifierSignatureB64": null,
            "customerPasswordB64": null
        }
    };
}