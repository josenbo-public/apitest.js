
function dibpsCryptoCreateKeys(nacl_instance) {

    if (!dibpsData.device.keySeedB64) {
        dibpsData.device.keySeedBytes = nacl_instance.random_bytes(32);
        dibpsData.device.keyUuid = uuidv4();
        dibpsData.device.keySeedB64 = BytesToBase64(dibpsData.device.keySeedBytes);
    }

    // Ein Schlüsselpaar zum Signieren
    // https://github.com/tonyg/js-nacl#naclcrypto_sign_seed_keypairuint8array--signpk-uint8array-signsk-uint8array
    dibpsData.device.keyPairSign = nacl_instance.crypto_sign_seed_keypair(dibpsData.device.keySeedBytes);

    // Ein Schlüsselpaar für die asymmetrische Verschlüsselung
    // https://github.com/tonyg/js-nacl#naclcrypto_box_seed_keypairuint8array--boxpk-uint8array-boxsk-uint8array
    dibpsData.device.keyPairCrypt = nacl_instance.crypto_box_seed_keypair(dibpsData.device.keySeedBytes);

    dibpsData.device.publicKey64 = BytesToBase64(dibpsData.device.keyPairSign.signPk);
}


async function dibpsCryptoRegisterRegToken(nacl, messages) {

    let challengeBytes = Base64ToBytes(dibpsData.input.challengeB64);
    let tokenBytes = Base64ToBytes(dibpsData.input.regTokenB64);

    if (tokenBytes.length !== 64) {
        messages.push("Das RegToken sah gut aus, bildete aber nicht genau die erwarteten 64 Bytes ab");
        return null;
    }

    let verifierSignatureBytes = null;
    let tokenSignatureBytes = null;

    let ks = tokenBytes.slice(0, 32);
    let kp = tokenBytes.slice(32, 64);

    let key = await window.crypto.subtle.importKey(
        "raw", // raw format of the key - should be Uint8Array
        ks,
        { // algorithm details
            name: "HMAC",
            hash: {name: "SHA-256"}
        },
        false, // export = false
        ["sign", "verify"] // what this key can do
    );

    let signature = await  window.crypto.subtle.sign(
            "HMAC",
            key,
            challengeBytes
        );

    let verifierBytes = new Uint8Array(signature);
    // let str = Array.prototype.map.call(b, x => ('00' + x.toString(16)).slice(-2)).join("")
    let verifierB64 = BytesToBase64(verifierBytes);

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        verifierSignatureBytes = nacl.crypto_sign_detached(verifierBytes, dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren des Verifiers (crypto_sign_detached): " + e.message);
        return null;
    }

    dibpsData.output.verifierB64 = verifierB64;
    dibpsData.output.verifierSignatureB64 = BytesToBase64(verifierSignatureBytes);
    dibpsData.output.customerPasswordB64 = BytesToBase64(kp);
}

function dibpsCryptoRegisterPeerDevice(nacl, messages) {

    let challengeBytes = Base64ToBytes(dibpsData.input.challengeB64);
    let tokenBytes = StringToUtf8Bytes(dibpsData.input.authToken);
    let challengeSignatureBytes = null;
    let tokenSignatureBytes = null;

    dibpsData.output.challengeSignatureB64 = null;
    dibpsData.output.authTokenSignatureB64 = null;

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        challengeSignatureBytes = nacl.crypto_sign_detached(challengeBytes, dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren der Challenge (crypto_sign_detached): " + e.message)
        return null;
    }

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        tokenSignatureBytes = nacl.crypto_sign_detached(concatTypedArrays(tokenBytes, challengeBytes), dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren des AuthTokens (crypto_sign_detached): " + e.message)
        return null;
    }

    dibpsData.output.challengeSignatureB64 = BytesToBase64(challengeSignatureBytes);
    dibpsData.output.authTokenSignatureB64 = BytesToBase64(tokenSignatureBytes);
}

function dibpsCryptoRegisterVerifyToken(nacl, messages) {

    let challengeBytes = Base64ToBytes(dibpsData.input.challengeB64);
    let tokenBytesFromBytes = Base64ToBytes(dibpsData.input.verifyTokenB64);
    let tokenBytesFromString = StringToUtf8Bytes(dibpsData.input.verifyTokenB64);
    let challengeSignatureBytes = null;
    let tokenSignatureBytesFromBytes = null;
    let tokenSignatureBytesFromString = null;

    dibpsData.output.challengeSignatureB64 = null;
    dibpsData.output.cardNumberSignatureB64 = null;

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        challengeSignatureBytes = nacl.crypto_sign_detached(challengeBytes, dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren der Challenge (crypto_sign_detached): " + e.message)
        return null;
    }

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        tokenSignatureBytesFromBytes = nacl.crypto_sign_detached(concatTypedArrays(tokenBytesFromBytes, challengeBytes), dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren des VerifyTokens Byte-Variante (crypto_sign_detached): " + e.message)
        return null;
    }

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        tokenSignatureBytesFromString = nacl.crypto_sign_detached(concatTypedArrays(tokenBytesFromString, challengeBytes), dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren des VerifyTokens UTF8-String-Bytes-Variante (crypto_sign_detached): " + e.message)
        return null;
    }

    dibpsData.output.challengeSignatureB64 = BytesToBase64(challengeSignatureBytes);
    dibpsData.output.verifyTokenSignatureFromBytesB64 = BytesToBase64(tokenSignatureBytesFromBytes);
    dibpsData.output.verifyTokenSignatureFromStringB64 = BytesToBase64(tokenSignatureBytesFromString);
}

function dibpsCryptoRegisterCardNumber(nacl, messages) {

    let challengeBytes = Base64ToBytes(dibpsData.input.challengeB64);
    let tokenBytes = StringToUtf8Bytes(dibpsData.input.cardNumber);
    let challengeSignatureBytes = null;
    let tokenSignatureBytes = null;

    dibpsData.output.challengeSignatureB64 = null;
    dibpsData.output.cardNumberSignatureB64 = null;

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        challengeSignatureBytes = nacl.crypto_sign_detached(challengeBytes, dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren der Challenge (crypto_sign_detached): " + e.message)
        return null;
    }

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        tokenSignatureBytes = nacl.crypto_sign_detached(concatTypedArrays(tokenBytes, challengeBytes), dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren der Kartennummer (crypto_sign_detached): " + e.message)
        return null;
    }

    dibpsData.output.challengeSignatureB64 = BytesToBase64(challengeSignatureBytes);
    dibpsData.output.cardNumberSignatureB64 = BytesToBase64(tokenSignatureBytes);
}

function dibpsCryptoRegisterDirect(nacl, messages) {

    let challengeBytes = Base64ToBytes(dibpsData.input.challengeB64);
    let signatureBytes = null;
    dibpsData.output.challengeSignatureB64 = null;

    try {
        // https://github.com/tonyg/js-nacl#naclcrypto_sign_detachedmsgbin-signersecretkey--uint8array
        signatureBytes = nacl.crypto_sign_detached(challengeBytes, dibpsData.device.keyPairSign.signSk);
    } catch (e) {
        messages.push("Exception beim Signieren (crypto_sign_detached): " + e.message)
        return null;
    }

    dibpsData.output.challengeSignatureB64 = BytesToBase64(signatureBytes);
}
