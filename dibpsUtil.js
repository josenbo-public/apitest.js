

function StringToUtf8Bytes(theString) {
    return new TextEncoder().encode(theString);
}

/**
 * @return {string}
 */
function Utf8BytesToString(thUtf8Bytes) {
    return new TextDecoder().decode(thUtf8Bytes);
}

function BytesToBase64(theByteArray) {
    return window.Base64.fromUint8Array(theByteArray);
}

function Base64ToBytes(theBase64) {
    return window.Base64.toUint8Array(theBase64);
}

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function prependKeyUuid(otherPart) {
    return dibpsData.device.keyUuid + "." + otherPart;
}

// https://stackoverflow.com/questions/33702838/how-to-append-bytes-multi-bytes-and-buffer-to-arraybuffer-in-javascript
function concatTypedArrays(a, b) { // a, b TypedArray of same type
    var c = new (a.constructor)(a.length + b.length);
    c.set(a, 0);
    c.set(b, a.length);
    return c;
}