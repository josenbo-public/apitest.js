
function dibpsUIActivateSection(sectionIndex) {

    function setElementInnerText(theIdSuffix, theText) {
        let theElement = document.getElementById(dibpsData.section.name + "-" + theIdSuffix);
        if (! theElement) return;
        let oldText = theElement.innerText;
        if (oldText === theText) return;
        theElement.innerText = theText;
    }

    function setTextInputValue(theIdSuffix, theText) {
        let theElement = document.getElementById(dibpsData.section.name + "-" + theIdSuffix);
        if (! theElement) return;
        let oldText = theElement.value;
        if (oldText === theText) return;
        theElement.value = theText;
    }

    const sectionNames = [
        "Pos0notUsed",
        "sectionDevice",
        "sectionRegToken",
        "sectionPeerDevice",
        "sectionVerifyToken",
        "sectionCardNumber",
        "sectionDirect"
    ];

    let theChallengeElement = document.getElementById(dibpsData.section.name + "-challenge");
    if (theChallengeElement) {
        dibpsData.input.challengeB64 = theChallengeElement.value;
    }

    if (sectionIndex < 1 || 6 < sectionIndex) {
        alert("Die Seite " + sectionIndex + " existiert nicht");
        return null;
    }

    dibpsData.section.id = sectionIndex;
    dibpsData.section.name = sectionNames[sectionIndex];

    for (let i = 1; i <= 6; i++) {
        document.getElementById(sectionNames[i]).hidden = (i !== sectionIndex);
    }

    setElementInnerText("pubKeyAndUuid", prependKeyUuid(dibpsData.device.publicKey64));

    setTextInputValue("challenge", dibpsData.input.challengeB64);
}

function dibpsUIInitializeApp(theUrlParam) {

    function doInitialization(nacl, url) {

        dibpsDataInitialize();

        if (url.searchParams.has("seed") && url.searchParams.has("uuid")) {
            let seedParam = url.searchParams.get("seed");
            let uuidParam = url.searchParams.get("uuid");
            if (/^[a-zA-Z0-9+/]{43}=$/.test(seedParam) && /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/.test(uuidParam)) {
                dibpsData.device.keySeedB64 = seedParam;
                dibpsData.device.keySeedBytes = Base64ToBytes(seedParam);
                dibpsData.device.keyUuid = uuidParam;
            }
        }

        dibpsCryptoCreateKeys(nacl);
        
        url.searchParams.delete("seed");
        url.searchParams.append("seed", dibpsData.device.keySeedB64);
        url.searchParams.delete("uuid");
        url.searchParams.append("uuid", dibpsData.device.keyUuid);

        dibpsData.device.bookmark = url.toString();
    
        document.getElementById("sectionDevice-seed").innerText = dibpsData.device.keySeedB64;
        document.getElementById("sectionDevice-pubkey").innerText = dibpsData.device.publicKey64;
        document.getElementById("sectionDevice-uuid").innerText = dibpsData.device.keyUuid;
        document.getElementById("sectionDevice-bookmark").href = dibpsData.device.bookmark;
    
        // dibpsUIActivateSection(1);
    };

    window.nacl_factory.instantiate(function (theNaclInstance) {
        doInitialization(theNaclInstance, theUrlParam);
    });
}

async function dibpsUICalcRegisterRegToken() {

    async function doWork(nacl, sectionId, sectionName) {
        let messages = [];

        document.getElementById(sectionName + "-results").hidden = true;
        document.getElementById(sectionName + "-errors").hidden = true;

        let challenge = document.getElementById(sectionName + "-challenge").value;
        let token = document.getElementById(sectionName + "-regToken").value;

        if (challenge.trim().length = 0) {
            messages.push("Die Challenge muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]+={0,3}$/.test(challenge)) {
            messages.push("Die Challlenge sieht nicht aus wie ein Base64-kodierter Wert (URL-Safe Kodierung vereinbart)");
        }

        if (challenge.length < 44) {
            messages.push("Die Challenge muss mindestens 44 Zeichen lang sein (32 Byte Mindestlänge für Challenge vereinbart)");
        }

        if ((challenge.length % 4) != 0) {
            messages.push("Die Länge der Challenge muss glatt durch 4 teilbar sein (Padding vereinbart)");
        }

        if (token.trim().length == 0) {
            messages.push("Das RegToken muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]{86}==$/.test(token)) {
            messages.push("Das RegToken sieht nicht aus wie ein Base64-kodierter Byte[64] aus (Länge 64 Byte, URL-Safe Encoding mit Padding vereinbart)");
        }

        if (0 == messages.length) {

            dibpsData.input.challengeB64 = challenge;
            dibpsData.input.regTokenB64 = token;

            await dibpsCryptoRegisterRegToken(nacl, messages);
        }

        if (0 < messages.length) {

            document.getElementById(sectionName + "-errors").hidden = false;

            let list = document.getElementById(sectionName + "-errorList");

            list.innerHTML = "";

            for (let i = 0; i < messages.length; i++) {
                let item = document.createElement("li");
                item.setAttribute("class", "warning");
                item.innerText = messages[i];
                list.appendChild(item);
            }

        } else {

            document.getElementById(sectionName + "-results").hidden = false;
            document.getElementById(sectionName + "-verifier").innerText = dibpsData.output.verifierB64;
            document.getElementById(sectionName + "-verifierSignature").innerText = prependKeyUuid(dibpsData.output.verifierSignatureB64);
            document.getElementById(sectionName + "-customerPassword").innerText = dibpsData.output.customerPasswordB64;

        }
    }

    await window.nacl_factory.instantiate(async function (theNaclInstance) {
        await doWork(theNaclInstance, dibpsData.section.id, dibpsData.section.name);
    });
}

function dibpsUICalcRegisterPeerDevice() {

    function doWork(nacl, sectionId, sectionName) {
        let messages = [];

        document.getElementById(sectionName + "-results").hidden = true;
        document.getElementById(sectionName + "-errors").hidden = true;

        let challenge = document.getElementById(sectionName + "-challenge").value;
        let token = document.getElementById(sectionName + "-authToken").value;

        if(challenge.trim().length = 0) {
            messages.push("Die Challenge muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]+={0,3}$/.test(challenge)) {
            messages.push("Die Challlenge sieht nicht aus wie ein Base64-kodierter Wert (URL-Safe Kodierung vereinbart)");
        }

        if(challenge.length < 44) {
            messages.push("Die Challenge muss mindestens 44 Zeichen lang sein (32 Byte Mindestlänge für Challenge vereinbart)");
        }

        if ((challenge.length % 4) != 0) {
            messages.push("Die Länge der Challenge muss glatt durch 4 teilbar sein (Padding vereinbart)");
        }

        if (token.trim().length == 0){
            messages.push("Das AuthToken muss angegeben werden");
        }

        if (!/^[0-9]{10}$/.test(token)) {
            messages.push("Das AuthToken muss aus 10 Ziffern bestehen");
        }

        if (0 == messages.length) {

            dibpsData.input.challengeB64 = challenge;
            dibpsData.input.authToken = token;

            dibpsCryptoRegisterPeerDevice(nacl, messages);
        }

        if (0 < messages.length) {

            document.getElementById(sectionName + "-errors").hidden = false;

            let list = document.getElementById(sectionName + "-errorList");

            list.innerHTML = "";

            for (let i = 0; i < messages.length; i++) {
                let item = document.createElement("li");
                item.setAttribute("class", "warning");
                item.innerText = messages[i];
                list.appendChild(item);
            }

        } else {

            document.getElementById(sectionName + "-results").hidden = false;
            document.getElementById(sectionName + "-challengeSignature").innerText = prependKeyUuid(dibpsData.output.challengeSignatureB64);
            document.getElementById(sectionName + "-tokenSignature").innerText = prependKeyUuid(dibpsData.output.authTokenSignatureB64);

        }
    }

    window.nacl_factory.instantiate(function (theNaclInstance) {
        doWork(theNaclInstance, dibpsData.section.id, dibpsData.section.name);
    });
}

function dibpsUICalcRegisterVerifyToken() {

    function doWork(nacl, sectionId, sectionName) {
        let messages = [];

        document.getElementById(sectionName + "-results").hidden = true;
        document.getElementById(sectionName + "-errors").hidden = true;

        let challenge = document.getElementById(sectionName + "-challenge").value;
        let token = document.getElementById(sectionName + "-verifyToken").value;

        if(challenge.trim().length = 0) {
            messages.push("Die Challenge muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]+={0,3}$/.test(challenge)) {
            messages.push("Die Challlenge sieht nicht aus wie ein Base64-kodierter Wert (URL-Safe Kodierung vereinbart)");
        }

        if(challenge.length < 44) {
            messages.push("Die Challenge muss mindestens 44 Zeichen lang sein (32 Byte Mindestlänge für Challenge vereinbart)");
        }

        if ((challenge.length % 4) != 0) {
            messages.push("Die Länge der Challenge muss glatt durch 4 teilbar sein (Padding vereinbart)");
        }

        if (token.trim().length == 0){
            messages.push("Das VerifyToken muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]+={0,3}$/.test(token)) {
            messages.push("Das VerifyToken sieht nicht aus wie ein Base64-kodierter Wert (URL-Safe Kodierung vereinbart)");
        }

        if ((token.length % 4) != 0) {
            messages.push("Die Länge des VerifyTokens muss glatt durch 4 teilbar sein (Padding vereinbart)");
        }


        if (0 == messages.length) {

            dibpsData.input.challengeB64 = challenge;
            dibpsData.input.verifyTokenB64 = token;

            dibpsCryptoRegisterVerifyToken(nacl, messages);
        }

        if (0 < messages.length) {

            document.getElementById(sectionName + "-errors").hidden = false;

            let list = document.getElementById(sectionName + "-errorList");

            list.innerHTML = "";

            for (let i = 0; i < messages.length; i++) {
                let item = document.createElement("li");
                item.setAttribute("class", "warning");
                item.innerText = messages[i];
                list.appendChild(item);
            }

        } else {

            document.getElementById(sectionName + "-results").hidden = false;
            document.getElementById(sectionName + "-challengeSignature").innerText = prependKeyUuid(dibpsData.output.challengeSignatureB64);
            document.getElementById(sectionName + "-verifyTokenAsParam1").innerText = token;
            document.getElementById(sectionName + "-tokenSignatureFromString").innerText = prependKeyUuid(dibpsData.output.verifyTokenSignatureFromStringB64);
            document.getElementById(sectionName + "-verifyTokenAsParam2").innerText = token;
            document.getElementById(sectionName + "-tokenSignatureFromBytes").innerText = prependKeyUuid(dibpsData.output.verifyTokenSignatureFromBytesB64);

        }
    }

    window.nacl_factory.instantiate(function (theNaclInstance) {
        doWork(theNaclInstance, dibpsData.section.id, dibpsData.section.name);
    });
}

function dibpsUICalcRegisterCardNumber() {

    function doWork(nacl, sectionId, sectionName) {
        let messages = [];

        document.getElementById(sectionName + "-results").hidden = true;
        document.getElementById(sectionName + "-errors").hidden = true;

        let challenge = document.getElementById(sectionName + "-challenge").value;
        let token = document.getElementById(sectionName + "-cardNumber").value;

        if(challenge.trim().length = 0) {
            messages.push("Die Challenge muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]+={0,3}$/.test(challenge)) {
            messages.push("Die Challlenge sieht nicht aus wie ein Base64-kodierter Wert (URL-Safe Kodierung vereinbart)");
        }

        if(challenge.length < 44) {
            messages.push("Die Challenge muss mindestens 44 Zeichen lang sein (32 Byte Mindestlänge für Challenge vereinbart)");
        }

        if ((challenge.length % 4) != 0) {
            messages.push("Die Länge der Challenge muss glatt durch 4 teilbar sein (Padding vereinbart)");
        }

        if (token.trim().length == 0){
            messages.push("Die Kartennummer muss angegeben werden");
        }

        if (!/^[0-9]+$/.test(token)) {
            messages.push("Die Kartennummer darf nur aus Ziffern bestehen");
        }

        if (0 == messages.length) {

            dibpsData.input.challengeB64 = challenge;
            dibpsData.input.cardNumber = token;

            dibpsCryptoRegisterCardNumber(nacl, messages);
        }

        if (0 < messages.length) {

            document.getElementById(sectionName + "-errors").hidden = false;

            let list = document.getElementById(sectionName + "-errorList");

            list.innerHTML = "";

            for (let i = 0; i < messages.length; i++) {
                let item = document.createElement("li");
                item.setAttribute("class", "warning");
                item.innerText = messages[i];
                list.appendChild(item);
            }

        } else {

            document.getElementById(sectionName + "-results").hidden = false;
            document.getElementById(sectionName + "-challengeSignature").innerText = prependKeyUuid(dibpsData.output.challengeSignatureB64);
            document.getElementById(sectionName + "-tokenSignature").innerText = prependKeyUuid(dibpsData.output.cardNumberSignatureB64);

        }
    }

    window.nacl_factory.instantiate(function (theNaclInstance) {
        doWork(theNaclInstance, dibpsData.section.id, dibpsData.section.name);
    });
}

function dibpsUICalcRegisterDirect() {

    function doWork(nacl, sectionId, sectionName) {
        let messages = [];

        document.getElementById(sectionName + "-results").hidden = true;
        document.getElementById(sectionName + "-errors").hidden = true;

        let challenge = document.getElementById(sectionName + "-challenge").value;

        if(challenge.trim().length = 0) {
            messages.push("Die Challenge muss angegeben werden");
        }

        if (!/^[a-zA-Z0-9+/]+={0,3}$/.test(challenge)) {
            messages.push("Die Challlenge sieht nicht aus wie ein Base64-kodierter Wert (URL-Safe Kodierung vereinbart");
        }

        if(challenge.length < 44) {
            messages.push("Die Challenge muss mindestens 44 Zeichen lang sein (32 Byte Mindestlänge für Challenge vereinbart)");
        }

        if ((challenge.length % 4) != 0) {
            messages.push("Die Länge der Challenge muss glatt durch 4 teilbar sein (Padding vereinbart)");
        }

        if (0 == messages.length) {

            dibpsData.input.challengeB64 = challenge;

            dibpsCryptoRegisterDirect(nacl, messages);
        }

        if (0 < messages.length) {

            document.getElementById(sectionName + "-errors").hidden = false;

            let list = document.getElementById(sectionName + "-errorList");

            list.innerHTML = "";

            for (let i = 0; i < messages.length; i++) {
                let item = document.createElement("li");
                item.setAttribute("class", "warning");
                item.innerText = messages[i];
                list.appendChild(item);
            }

        } else {

            document.getElementById(sectionName + "-results").hidden = false;
            document.getElementById(sectionName + "-challengeSignature").innerText = prependKeyUuid(dibpsData.output.challengeSignatureB64);

        }
    }

    window.nacl_factory.instantiate(function (theNaclInstance) {
        doWork(theNaclInstance, dibpsData.section.id, dibpsData.section.name);
    });
}

